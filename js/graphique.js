
// percentage_per_year
var p = 5
// c
var c = Math.log(1+p/100)

// value after t years
function conv(t) {
  return 1 - Math.exp(-c*t);
}

// array version of conv
function a_conv(T) {
	R = []
	for (i=0; i<T.length; i++) {R.push(conv(T[i]))};
	return R
}
// time
var T = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80]

var T_label = [0, '', '', '', '', '', '', '', '', '', 10, '', '', '', '', '', '', '', '', '', 20, '', '', '', '', '', '', '', '', '', 30, '', '', '', '', '', '', '', '', '', 40, '', '', '', '', '', '', '', '', '', 50, '', '', '', '', '', '', '', '', '', 60, '', '', '', '', '', '', '', '', '', 70, '', '', '', '', '', '', '', '', '', 80]
// average power (M/N)
var MN = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

// create the two lines
var lineChartData = {
	labels: T_label,
	datasets: [{
		label: 'pouvoir relatif',
		borderColor: window.chartColors.red,
		backgroundColor: window.chartColors.red,
		fill: false,
		data: a_conv(T),
	}, {
		label: 'pouvoir moyen (M/N)',
		borderColor: window.chartColors.blue,
		backgroundColor: window.chartColors.blue,
		fill: false,
		data: MN,
	}]
};

var options = {
	data: lineChartData,
	options: {
		responsive: true,
		hoverMode: 'index',
		stacked: false,
		title: {
			display: true,
			text: 'Convergence vers la moyenne'
		},
		scales: {
			xAxes: [{
				display: true,
				scaleLabel: {
					display: true,
					labelString: 'temps (années)'
				}
			}],
			yAxes: [{
				display: true,
				scaleLabel: {
					display: true,
					labelString: 'proportion atteinte du pouvoir moyen'
				}
			}],
		}
	}
}

// draw on load
window.onload = function() {
	var ctx = document.getElementById('canvas').getContext('2d');
	window.myLine = Chart.Line(ctx, options);
};

// change pointer when hovering slider
document.getElementById("rangeInput").style.cursor = "pointer";

// text area for percentage_per_year display
var percentage_per_year_display = document.getElementById('percentage_per_year')

// when the value of p has changed, refresh the curve
function actualizeCurve() {
	c = Math.log(1+p/100)
	lineChartData.datasets[0].data = a_conv(T)
	window.myLine.update();
	percentage_per_year_display.innerHTML = ((Math.exp(c) - 1) * 100).toPrecision(3)
}

// addEventListener to the value fields
document.getElementById('rangeInput').addEventListener('input', function (evt) {
p = this.value
	actualizeCurve()
});
document.getElementById('amount').addEventListener('input', function (evt) {
p = this.value
	actualizeCurve()
});

